import React, { Component } from 'react';

import Header from './subcomponents/Header.js';
import ApplicationComponent from './ApplicationComponent.js';
//import Footer from './Footer.js';

class GroupTunesSkeleton extends Component {
  constructor() {
      super();
      this.state = {
        currentUser: "Software Engineering"
      }
  }


  render() {
    return (
      <div>
        <Header currentUser={this.state.currentUser}/>
        <ApplicationComponent currentUser={this.state.currentUser}/>
      </div>
    )
  }
}

export default GroupTunesSkeleton;
