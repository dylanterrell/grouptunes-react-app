import React, { Component } from 'react';

class InputForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSong: "",
      currentArtist: "",
      currentUser: ""
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    this.props.createSongItem(e);
    this.setState({
      currentSong: "",
      currentArtist: "",
      currentUser: ""
    });
  }

  render() {
    return (
      <section className='add-item'>
          <form onSubmit={this.handleSubmit}>
            <h4>Add a new song</h4>
            <input type="text" name="currentSong" placeholder="What's the song title?" onChange={this.handleChange} value={this.state.currentSong}/>
            <input type="text" name="currentArtist" placeholder="Who's the artist?" onChange={this.handleChange} value={this.state.currentArtist}/>
            <input type="text" name="username" placeholder="What's your name?" onChange={this.handleChange} value={this.state.username}/>
            <button>Submit</button>
          </form>
          <button onClick={this.props.deleteSongs} className="deletebtn">Remove Songs</button>
      </section>
    )
  }
}

export default InputForm;
