import React from 'react';

import logo from '../../logo.png';

const Header = ({currentUser}) => {
  return (
    <div className="header">
      <h1>Grouptunes</h1>
      <img src={logo} alt="logo" />
      <h3>{currentUser}</h3>
    </div>
  )
}

export default Header;
