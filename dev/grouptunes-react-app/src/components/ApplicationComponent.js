import React, { Component } from 'react';
import { Container } from 'react-grid-system';

import InputForm from './subcomponents/InputForm.js';

class ApplicationComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      currentSong: "",
      currentArtist: "",
      items: []
    }
    this.createSongItem = this.createSongItem.bind(this);
    this.updateSongItem = this.updateSongItem.bind(this);
    this.deleteSongs = this.deleteSongs.bind(this);
  }

  createSongItem(e) {
    e.preventDefault();
    this.setState({ currentUser: e.target.username.value });
    const newItem = {
      id: Math.floor((Math.random() * 10000) + 1),
      currentSong: e.target.currentSong.value,
      currentArtist: e.target.currentArtist.value,
      username: e.target.username.value
    };
    let currentItems = this.state.items;
    currentItems.unshift(newItem);
    this.setState({ items: currentItems });
  }

  updateSongItem() {

  }

  deleteSongs() {
    let currentItems = [];
    this.setState({ items: currentItems });
  }

  render() {
    return (
      <Container>
        <InputForm createSongItem={this.createSongItem} updateSongItem={this.updateSongItem} deleteSongs={this.deleteSongs}/>
        <section className='display-item'>
        <div className="wrapper">
          <ul>
            {this.state.items.map((item) => {
              return (
                <li key={item.id}>
                  <h3>Song: {item.currentSong}</h3>
                  <h3>Artist: {item.currentArtist}</h3>
                  <p>Recommended by: {item.username}</p>
                </li>
              )
            })}
          </ul>
        </div>
      </section>
      </Container>
    )
  }
}

export default ApplicationComponent;
