import React, { Component } from 'react';
import './App.css';

import GroupTunesSkeleton from './components/GroupTunesSkeleton.js'

class App extends Component {
  render() {
    return (
      <div>
        <GroupTunesSkeleton />
      </div>
    );
  }
}

export default App;
